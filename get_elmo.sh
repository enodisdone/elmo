#!/usr/bin/env bash

echo "=== Starting the script ==="
echo "---"

mkdir -p elmo_trained
cd elmo_trained

## Download the dataset
if [[ ! -d 'elmo_trained' ]]; then
    echo "- Downloading japanese pre-trained ELMo model"
    wget --continue https://elmoja.blob.core.windows.net/elmoweights/weights.hdf5
    wget --continue https://elmoja.blob.core.windows.net/elmoweights/options.json
fi