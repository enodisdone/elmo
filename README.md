# Instructions

## Overview

This package is for reproducing the ELMo based results of our paper - [An Investigation of Transfer Learning-Based Sentiment Analysis in Japanese](https://arxiv.org/abs/1905.09642) 

It is based on AllenNLP research library, built on PyTorch, for developing state-of-the-art deep learning models on a wide variety of linguistic tasks.

## Installation
`pip install -r requirements.txt`

To install mecab and mecab-ipadic dictionary, refer to the following links
 - [mecab](http://taku910.github.io/mecab/)
 - [mecab-ipadic](https://github.com/neologd/mecab-ipadic-neologd/blob/master/README.ja.md)

## Download pre-trained ELMo
`bash get_elmo.sh` 
 
Script creates `elmo-trained` folder with our pre-trained ELMo hdf5 model file and options.json files in it. 

## Training
AllenNLP provides nice command line tool and all the configurations can be specified in json file. To train, run:
```
allennlp train \
experiments/classify_yahoo.json \
-s /tmp/yahoo_output \
--include-package elmo_jp
```
Training script will show the progress and finish with following format:
```
2019-01-08 20:35:58,300 - INFO - allennlp.common.util - Metrics: {
  "training_duration": "09:27:50",
  "training_start_epoch": 0,
  "training_epochs": 9,
  "epoch": 9,
  "training_accuracy": 0.9157390734981176,
  "training_accuracy3": 1.0,
  "training_loss": 0.2126120420792067,
  "validation_accuracy": 0.9096415125225078,
  "validation_accuracy3": 1.0,
  "validation_loss": 0.2303196048042225,
  "best_epoch": 9,
  "best_validation_accuracy": 0.9096415125225078,
  "best_validation_accuracy3": 1.0,
  "best_validation_loss": 0.2303196048042225
}
``` 
## Evaluation
To evaluate on the test set after training, pass the `evaluate_on_test` flag, or use the `allennlp evaluate` command.

## Configuration
To make modifications, refer to `experiments/classify_yahoo.json` where all the model, hyperparameters and training files location has been set.

### Training on your own dataset
All you need to change is `X_data_path` of `classify_yahoo.json` for train, validation and test dataset accordingly. 

In case your files have not been tokenized already, you have to set `self._tokenizer` to `MecabTokenizer` instead of `WordTokenizer` in `dataset_readers/readers.py`  

## Citation
For now please cite our paper on arxiv
```
@ARTICLE{2019arXiv190509642B,
       author = {{Bataa}, Enkhbold and {Wu}, Joshua},
        title = "{An Investigation of Transfer Learning-Based Sentiment Analysis in Japanese}",
      journal = {arXiv e-prints},
     keywords = {Computer Science - Computation and Language, Computer Science - Machine Learning},
         year = "2019",
        month = "May",
          eid = {arXiv:1905.09642},
        pages = {arXiv:1905.09642},
archivePrefix = {arXiv},
       eprint = {1905.09642},
 primaryClass = {cs.CL},
       adsurl = {https://ui.adsabs.harvard.edu/abs/2019arXiv190509642B},
      adsnote = {Provided by the SAO/NASA Astrophysics Data System}
}
```