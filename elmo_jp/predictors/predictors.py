from typing import Tuple

from overrides import overrides

from allennlp.common.util import JsonDict
from allennlp.data import Instance
from allennlp.predictors.predictor import Predictor


# @Predictor.register('sequence_classifier')
# class SequenceClassifierPredictor(Predictor):
#     """
#     Wrapper for the :class:`~custom.models.SequenceClassifier` model.
#     """
#     @overrides
#     def _json_to_instance(self, json: JsonDict) -> Instance:
#         """
#         Expects JSON that looks like ``{"text": "..."}``.
#         """
#         input_text = json["text"]
#         return self._dataset_reader.text_to_instance(input_text)

@Predictor.register('review-classifier')
class PaperClassifierPredictor(Predictor):
    """Predictor wrapper for the AcademicPaperClassifier"""
    @overrides
    def _json_to_instance(self, json_dict: JsonDict) -> Tuple[Instance, JsonDict]:
        review = json_dict['review']
        instance = self._dataset_reader.text_to_instance(tokens=review)

        # label_dict will be like {0: "ACL", 1: "AI", ...}
        # label_dict = self._model.vocab.get_index_to_token_vocabulary('labels')
        # Convert it to list ["ACL", "AI", ...]
        # all_labels = [label_dict[i] for i in range(len(label_dict))]

        return instance#, {"all_labels": all_labels}
