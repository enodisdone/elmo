from typing import Dict
import logging

from overrides import overrides

import csv
from allennlp.common.file_utils import cached_path
from allennlp.data.dataset_readers.dataset_reader import DatasetReader
from allennlp.data.fields import LabelField, TextField
from allennlp.data.instance import Instance
from allennlp.data.tokenizers import Tokenizer, WordTokenizer
from allennlp.data.token_indexers import TokenIndexer, SingleIdTokenIndexer
from elmo_jp.mecab_tokenizer import MecabTokenizer, MecabSplitter

logger = logging.getLogger(__name__)  # pylint: disable=invalid-name


@DatasetReader.register("yahoo_csv_reader")
class YahoojpMovieReviewReader(DatasetReader):
    """
    Reads a JSON-lines file containing papers from the Semantic Scholar database, and creates a
    dataset suitable for document classification using these papers.

    Expected format for each input line: {"paperAbstract": "text", "title": "text", "venue": "text"}

    The JSON could have other fields, too, but they are ignored.

    The output of ``read`` is a list of ``Instance`` s with the fields:
        title: ``TextField``
        abstract: ``TextField``
        label: ``LabelField``

    where the ``label`` is derived from the venue of the paper.

    Parameters
    ----------
    lazy : ``bool`` (optional, default=False)
        Passed to ``DatasetReader``.  If this is ``True``, training will start sooner, but will
        take longer per batch.  This also allows training with datasets that are too large to fit
        in memory.
    tokenizer : ``Tokenizer``, optional
        Tokenizer to use to split the title and abstrct into words or other kinds of tokens.
        Defaults to ``WordTokenizer()``.
    token_indexers : ``Dict[str, TokenIndexer]``, optional
        Indexers used to define input token representations. Defaults to ``{"tokens":
        SingleIdTokenIndexer()}``.
    """

    def __init__(self,
                 lazy: bool = False,
                 tokenizer: Tokenizer = None,
                 tokens=None,
                 label=None,
                 skip_header: bool = False,
                 delimiter: str = ",",
                 token_indexers: Dict[str, TokenIndexer] = None) -> None:
        super().__init__(lazy)
        self._tokenizer = WordTokenizer()  # use MecabTokenizer(MecabSplitter()) for mecab
        self._input = tokens
        self._gold_label = label
        self._skip_header = skip_header
        self._delimiter = delimiter
        self._token_indexers = token_indexers or {"tokens": SingleIdTokenIndexer()}

    @overrides
    def _read(self, file_path):
        with open(cached_path(file_path), "r") as data_file:
            logger.info("Reading instances from lines in file at: %s", file_path)
            reader = csv.reader(data_file, delimiter=self._delimiter)
            if (self._skip_header):
                next(reader)
            for example in reader:
                tokens = example[self._input]
                label = example[self._gold_label]
                yield self.text_to_instance(tokens, label)

    @overrides
    def text_to_instance(self, tokens: str, label: str = None) -> Instance:  # type: ignore
        # pylint: disable=arguments-differ
        tokenized_review = self._tokenizer.tokenize(tokens)
        review_field = TextField(tokenized_review, self._token_indexers)
        fields = {'tokens': review_field}
        if label is not None:
            fields['label'] = LabelField(label)
        return Instance(fields)


# if __name__ == '__main__':
#     from allennlp.common.util import ensure_list
#     reader = YahoojpMovieReviewReader(review=0, label=1, skip_header=True)
#     instances = ensure_list(reader.read('/mnt/data/enod/Text-Classification-Elmo_jp/tests/fixtures/movie_reviews.csv'))
