# pylint: disable=no-self-use,invalid-name
from allennlp.common.testing import AllenNlpTestCase
from allennlp.common.util import ensure_list

from elmo_jp.dataset_readers import YahoojpMovieReviewReader


class TestSemanticScholarDatasetReader(AllenNlpTestCase):
    def test_read_from_file(self):
        reader = YahoojpMovieReviewReader(review=0, label=1, skip_header=True, lazy=True)
        instances = ensure_list(reader.read('/tests/fixtures/movie_reviews.csv'))
        instance1 = {"text": ["本当に", "感動", "し", "まし", "た", "。", "伊藤", "英明", "さん", "の", "かっこよ", "さ", "、", "勇敢", "さ", "、", "優し", "さ", "…", "など", "とにかく", "素敵", "でし", "た", "。", "この", "映画", "は", "恋愛", "も", "もちろん", "描か", "れ", "て", "い", "ます", "が", "、", "男", "同士", "の", "友情", "など", "も", "描か", "れ", "て", "おり", "、", "そこ", "に", "も", "また", "感動", "し", "まし", "た", "。", "時任", "三郎", "さん", "も", "素敵", "でし", "た", "。", "この", "映画", "は", "今", "まで", "の", "海", "猿", "を", "知ら", "なく", "て", "も", "かなり", "楽しめる", "ので", "、", "ぜひ", "観", "て", "いただき", "たい", "です", "。"],
                     "label": "0"}

        instance2 = {"text": ['を見れたのが意外な収穫でした♡\n\n邦画もたまには良いかな。'],
                     "label": "ACL"}

        instance3 = {"text": ['を見れたのが意外な収穫でした♡\n\n邦画もたまには良いかな。'],
                     "label": "ACL"}

        assert len(instances) == 10
        fields = instances[0].fields
        print(instances[0])
        print(f'PRINTING FIELDS {fields}')
        assert [t.text for t in fields["review"].tokens] == instance1["text"]
        # assert [t.text for t in fields["abstract"].tokens[:5]] == instance1["abstract"]
        # assert fields["label"].label == instance1["venue"]
        # fields = instances[1].fields
        # assert [t.text for t in fields["title"].tokens] == instance2["title"]
        # assert [t.text for t in fields["abstract"].tokens[:5]] == instance2["abstract"]
        # assert fields["label"].label == instance2["venue"]
        # fields = instances[2].fields
        # assert [t.text for t in fields["title"].tokens] == instance3["title"]
        # assert [t.text for t in fields["abstract"].tokens[:5]] == instance3["abstract"]
        # assert fields["label"].label == instance3["venue"]
