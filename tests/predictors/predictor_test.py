# pylint: disable=no-self-use,invalid-name,unused-import
from unittest import TestCase

from pytest import approx
from allennlp.models.archival import load_archive
from allennlp.predictors import Predictor

# required so that our custom model + predictor + dataset reader
# will be registered by name
import elmo_jp


class TestPaperClassifierPredictor(TestCase):
    def test_uses_named_inputs(self):
        inputs = {
            "review": "予告編 見 まし た が 、 L ！ そっくり です ね 。 あの 不気味 な 面構え 。 不 健康 そう な 顔 。 凄く はまっ て まし た し 、 しっかり 再現 さ れ て まし た 。 コレ は 、 マジ 楽しみ です 。 リューク も 気 に なっ て た の です が 、 ナルホド です 。 あ～ ～ 、 上映 が 待ち どう し い です ！"
        }

        archive = load_archive('tests/fixtures/model.tar.gz')
        predictor = Predictor.from_archive(archive, 'review-classifier')

        result = predictor.predict_json(inputs)

        label = result.get("label")
        assert label in ['0', '1']

        class_probabilities = result.get("class_probabilities")
        assert class_probabilities is not None
        assert all(cp > 0 for cp in class_probabilities)
        assert sum(class_probabilities) == approx(1.0)
